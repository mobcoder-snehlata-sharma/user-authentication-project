from django.urls import reverse_lazy, reverse
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth import login

from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView, CreateView, DetailView, UpdateView
from user_authentication_app.forms import (
    UserRegisterForm,
    UpdateProfileForm,
    PasswordResetForm,
)
from user_authentication_app.models import RegisterModel
from django.contrib.auth.views import PasswordChangeView


class HomeView(TemplateView):
    model = RegisterModel
    template_name = "registration/home.html"


class UserLoginView(LoginView):
    def get_success_url(self):
        return reverse("profile", kwargs={"pk": self.request.user.pk})


class RegisterView(CreateView):
    model = RegisterModel
    form_class = UserRegisterForm
    template_name = "registration/register.html"

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect("profile", pk=user.id)


class ProfileView(DetailView):
    model = RegisterModel
    template_name = "registration/profile.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProfileView, self).get_context_data(*args, **kwargs)
        page_user = get_object_or_404(RegisterModel, pk=self.kwargs["pk"])
        context["page_user"] = page_user
        return context


class UpdateProfileView(UpdateView):
    model = RegisterModel
    form_class = UpdateProfileForm
    template_name = "registration/update_profile.html"

    def get_context_data(self, *args, **kwargs):
        context = super(UpdateProfileView, self).get_context_data(*args, **kwargs)
        page_user = get_object_or_404(RegisterModel, pk=self.kwargs["pk"])
        context["page_user"] = page_user
        return context

    def get_success_url(self):
        return reverse("profile", kwargs={"pk": self.request.user.pk})


class ResetPassword(PasswordChangeView):
    model = RegisterModel
    form_class = PasswordResetForm

    def get_success_url(self):
        return reverse("profile", kwargs={"pk": self.request.user.pk})
