# Generated by Django 3.2.16 on 2022-10-28 07:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_authentication_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='registermodel',
            name='profile_image',
            field=models.ImageField(blank=True, null=True, upload_to='profile_images/'),
        ),
    ]
