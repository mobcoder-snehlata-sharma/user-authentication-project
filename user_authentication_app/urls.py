from django.urls import path
from user_authentication_app.views import (
    HomeView,
    RegisterView,
    ProfileView,
    UserLoginView,
    UpdateProfileView,
    ResetPassword,
)
from django.contrib.auth import views as auth_views


urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("register/", RegisterView.as_view(), name="register"),
    path("accounts/login/", UserLoginView.as_view(), name="login"),
    path("profile/<int:pk>", ProfileView.as_view(), name="profile"),
    path("profile_update/<int:pk>", UpdateProfileView.as_view(), name="update_profile"),
    path(
        "reset_password/<int:pk>",
        ResetPassword.as_view(template_name="registration/reset_password.html"),
        name="reset_password",
    ),
]
