from django import forms
from user_authentication_app.models import RegisterModel
from django.contrib.auth.forms import (
    UserCreationForm,
    UserChangeForm,
    PasswordChangeForm,
)
from django.db import transaction


class UserRegisterForm(UserCreationForm):

    name = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control"}))

    class Meta:
        model = RegisterModel
        fields = [
            "name",
            "date_of_birth",
            "email",
            "password1",
            "password2",
        ]

    @transaction.atomic
    def save(self):

        user = super().save(commit=False)
        user.name = self.cleaned_data.get("name")
        user.save()
        return user

    def __init__(self, *args, **kwargs):
        super(UserRegisterForm, self).__init__(*args, **kwargs)

        self.fields["email"].widget.attrs["class"] = "form-control"
        self.fields["password1"].widget.attrs["class"] = "form-control"
        self.fields["password2"].widget.attrs["class"] = "form-control"


class UpdateProfileForm(UserChangeForm):
    class Meta:
        model = RegisterModel
        fields = [
            "name",
            "date_of_birth",
            "email",
            "profile_image",
        ]


class PasswordResetForm(PasswordChangeForm):
    model = RegisterModel
    fields = ["old_password", "new_password1", "new_password2"]
