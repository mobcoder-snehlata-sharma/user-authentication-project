$(document).ready(function(){
    const image_button = document.getElementById("image-file");
    const camera_button = document.getElementById("camera-button");
    var token = $('[name="csrfmiddlewaretoken"]').attr('value');

    camera_button.addEventListener("click", function(){
      image_button.click();
    });

    image_button.onchange = evt => {
        const [file] = image_button.files
        if (file) {
            profile_img.src = URL.createObjectURL(file)
        }
    }
  });   